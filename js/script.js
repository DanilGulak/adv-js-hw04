fetch('https://swapi.dev/api/films/', {
    method: 'GET',
    headers: {
        'Content-type': 'application/json'
    }
})
    .then((r => r.json()))
    .then((d => d.results.forEach((e) => {

        const root = document.createElement('div')
        const filmContainer = document.createElement('div');
        const charContainer = document.createElement('ul');

        charContainer.textContent = 'Characters:'
        root.style.padding = '30px 0'

        document.body.append(root)
        root.append(filmContainer, charContainer);

        filmContainer.insertAdjacentHTML('beforeend', `<p>Title: ${e.title}</p><p>Episode: ${e.episode_id}</p><p>Crawl: ${e.opening_crawl}</p>`)

        const charArr = e.characters.map(el => fetch(el))

        Promise.all(charArr)
            .then(values => {
                values.forEach(elem => {
                    elem.json()
                        .then(data => data.name)
                        .then(name => {
                            const elem = document.createElement('li');
                            charContainer.append(elem);

                            elem.textContent = name;
                        })
                })
            })


    })))